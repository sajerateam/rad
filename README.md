
# React application dummy **(RAD)**

This boilerplate was created to accelerate the start of the project using "React.js".

## connected packages
- [React](https://reactjs.org)
- [Axios](https://www.npmjs.com/package/axios)
- [Lodash](https://lodash.com/docs)
- [dayjs](https://day.js.org/)
- [reactstrap](https://reactstrap.github.io)
- [react-router](https://reacttraining.com/react-router/web/example/basic)
- [react-redux-toastr](https://www.npmjs.com/package/react-redux-toastr)
- [react-redux](https://github.com/reactjs/react-redux)
- [redux-saga](https://redux-saga.js.org)
- [redux-form](https://redux-form.com)
- [redux-saga-controller](https://redux-saga-controller.js.org/)
- [fortawesome V5](https://fontawesome.com/start)


#### Fork
```
> git clone https://SPerekhrest@bitbucket.org/SPerekhrest/rad.git
```

#### install dependencies.
```
> npm install
```

#### Run
```
> npm run start
```
> For local development recommended to create file `.env.development.local` and copy content from `.env.development` file. That allows you to set up an environment for local development, including private data, to avoid sharing this data with co.
> 
#### Build

```
> npm run build
```

#### Tests

```
> npm run test
```
or
```
> npm run test:watch
```
