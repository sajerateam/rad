
// outsource dependencies
import chalk from 'chalk';
import webpack from 'webpack';

// local dependencies
import { ENV, checkRequiredFiles, environment, syncBrowser, configure, paths, varString, copyPublicFiles } from './config.mjs';


// configure
const NODE_ENV = process.env.NODE_ENV = varString(process.env.NODE_ENV) || ENV.PROD;

environment(NODE_ENV);

checkRequiredFiles([paths.html, paths.index]);

syncBrowser()
  .then(() => new Promise((resolve, reject) => {
    const config = configure(NODE_ENV);
    console.log(chalk.greenBright('V -'), chalk.green('Webpack ready'), '\n');
    console.log(chalk.cyan(`Creating "${NODE_ENV}" build...`), '\n');
    webpack(config).run((error, stats) => {
      // Configuration error
      if (error) { return reject(error); }
      const { errors, warnings } = stats.toJson({ all: false, warnings: true, errors: true });

      // console.log('// TODO', chalk.bgWhite(chalk.red(' ERRORS ')), errors, '\n', chalk.bgWhite(chalk.yellowBright(' WARNINGS ')), warnings);

      // Compilation error
      if (errors.length) { return reject(errors[0]); }
      return resolve(warnings);
    });
  }))
  .then(warnings => {
    copyPublicFiles();
    warnings.length ? console.log(chalk.yellow('Compiled with warnings.\n')) : console.log(chalk.greenBright('V -'), chalk.green('Compiled successfully.\n'));
    if (warnings.length) {
      console.log(
        warnings.map(warning => warning.message).join('\n\n'),
        '\n\n   Search for the', chalk.underline(chalk.yellow('keywords')), 'to learn more about each warning.',
        '\n   To ignore, add', chalk.cyan('// eslint-disable-next-line'), 'to the line before.\n'
      );
    }
  }).catch(error => console.log(chalk.redBright('Build failed with error:'), '\n', error.moduleName ? '\n\t' + error.moduleName + '\n' : '\n', '\n' + chalk.red(error.message), '\n\n'));
