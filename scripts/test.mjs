
// outsource dependencies
import jest from 'jest';

// local dependencies
import { ENV, checkRequiredFiles, environment, paths } from './config.mjs';


// configure
const NODE_ENV = process.env.NODE_ENV = process.env.NODE_ENV || ENV.TEST;

environment(NODE_ENV);

checkRequiredFiles([paths.html, paths.index]);

// delegate arguments from package.json
jest.run(process.argv.slice(2));
