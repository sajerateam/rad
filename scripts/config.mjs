'use strict';

// outsource dependencies
import open from 'open';
import fs from 'node:fs';
import chalk from 'chalk';
import dotenv from 'dotenv';
import path from 'node:path';
import { URL } from 'node:url';
import { expand } from 'dotenv-expand';
import browserslist from 'browserslist';
import DotenvWebpack from 'dotenv-webpack';
import { execSync } from 'node:child_process';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import TerserWebpackPlugin from 'terser-webpack-plugin';
import ESLintWebpackPlugin from 'eslint-webpack-plugin';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import CssMinimizerWebpackPlugin from 'css-minimizer-webpack-plugin';

// local dependencies


// configure process
process.stdout.write(process.platform === 'win32' ? '\x1B[2J\x1B[0f' : '\x1B[2J\x1B[5J\x1B[H');
// terminate the Node.js process with a non-zero exit code.
process.on('unhandledRejection', err => { throw err; });
process.on('uncaughtException', error => console.log(chalk.bold(chalk.redBright('Uncaught Exception:')), '\n\n  ', chalk.red(error), '\n\n') || process.exit(1));
const app = fs.realpathSync(process.cwd());
const resolve = relative => path.resolve(app, relative);
const nodeModule = relative => path.resolve(paths.modules, relative);
// ¯\_(ツ)_/¯
export const ENV = { TEST: 'test', PROD: 'production', DEV: 'development' };
export const urlFrom = ({ hostname, urlObject }) => String(Object.assign(new URL('http://localhost'), {
  hostname: (hostname === '0.0.0.0' || hostname === '::') ? 'localhost' : hostname,
  protocol: varBoolean(process.env.HTTPS) ? 'https' : 'http',
  port: varNumber(process.env.PORT),
}, urlObject));
export const varBoolean = value => !(/^(null|undefined|false|0)$/i.test(value) || !value);
export const varString = value => /^(null|undefined)$/i.test(value) ? void 0 : value;
export const varArray = value => value ? value.split(',') : void 0;
export const varNumber = value => parseFloat(value) || void 0;
/******************************************************
 *              PATH
 *****************************************************/
export const paths = {
  app,
  src: resolve('src'),
  index: resolve('src/index.js'),
  pub: resolve('public'),
  html: resolve('public/index.html'),
  build: resolve('build'),
  dotenv: resolve('.env'),
  scripts: resolve('scripts'),
  pkg: resolve('package.json'),
  modules: resolve('node_modules'),
  wpCache: resolve('node_modules/.cache'),
};
/******************************************************
 *   package.json content - getting package json for esm @see https://www.stefanjudis.com/snippets/how-to-import-json-files-in-es-modules-node-js/
 *****************************************************/
export const pkg = JSON.parse(fs.readFileSync(new URL('../package.json', import.meta.url)));
/*****************************************************************
 * ENV -> environment('development' | 'production' | 'test');
 * will apply to process.env bunch of files
 ****************************************************************/
export const environment = env => {
  console.log('\n' + chalk.cyan(`Configure "${env}" environment...`), '\n');
  [resolve(`.env.${env}.local`), resolve(`.env.${env}`), resolve('.env')]
    .forEach(path => fs.existsSync(path) && expand(dotenv.config({ path })));
  console.log(chalk.greenBright('V -'), chalk.green('Environment configured'), '\n');
};
/******************************************************
 *              ¯\_(ツ)_/¯
 *****************************************************/
export const checkRequiredFiles = files => {
  console.log(chalk.cyan('Checking files...'), '\n');
  let current;
  // NOTE at any case no sense to continue if the absent
  try { files.forEach(file => fs.accessSync(current = file, fs.constants.F_OK)); } catch (error) {
    console.log(chalk.red('Could not find a required file.'));
    console.log(chalk.red('\tName: ') + chalk.cyan(path.basename(current)));
    console.log(chalk.red('\tSearched in: ') + chalk.cyan(path.dirname(current)));
    process.exit(1);
  }
  console.log(chalk.greenBright('V -'), chalk.green('Files fine'), '\n');
};
/******************************************************
 *              ¯\_(ツ)_/¯
 *****************************************************/
export const copyPublicFiles = () => {
  console.log(chalk.cyan('Copy public files...'), '\n');
  // NOTE care about build folder
  // if (fs.existsSync(paths.build)) {
  //   fs.rmdirSync(paths.build, { recursive: true, force: true });
  // }
  // fs.mkdirSync(paths.build);
  // NOTE care about public files
  const files = fs.readdirSync(paths.pub).filter(file => /.*\.(js|json|ico|png|svg|jpg|pdf)/.test(file));
  for (const file of files) {
    fs.copyFileSync(path.join(paths.pub, file), path.join(paths.build, file));
  }
  // console.log('fs.readdirSync(paths.build)', fs.readdirSync(paths.build));
  // if(files) { throw new Error('process');}
  console.log(chalk.greenBright('V -'), chalk.green('Public files copied'), '\n');
}
/******************************************************
 *              ¯\_(ツ)_/¯
 *****************************************************/
export const syncBrowser = async () => {
  console.log(chalk.cyan('Sync browser...'), '\n');
  const current = browserslist.loadConfig({ path: paths.app })
  if (current == null) {
    throw new Error(`${chalk.red('As of react-scripts >=2 you must specify targeted browsers.') }
      Please add a ${chalk.underline('browserslist')} key to your ${chalk.bold('package.json')}.`);
  }
  browserslist.clearCaches();
  console.log(chalk.greenBright('V -'), chalk.green('Browser fine'), '\n');
};
/******************************************************
 *              ¯\_(ツ)_/¯
 *****************************************************/
// https://github.com/sindresorhus/open#app
const OSX_CHROME = 'google chrome';
export const openBrowser = url => {
  let browser = process.env.BROWSER;
  const args = process.env.BROWSER_ARGS ? process.env.BROWSER_ARGS.split(' ') : [];
  // NOTE Nothing special just stole from CRA
  if (process.platform === 'darwin' && (typeof browser !== 'string' || browser === OSX_CHROME)) {
    for (let chromium of ['Google Chrome Canary', 'Google Chrome', 'Microsoft Edge', 'Brave Browser', 'Vivaldi', 'Chromium']) {
      try {
        // Try our best to reuse existing tab on OSX Chromium-based browser with AppleScript
        execSync(`ps cax | grep "${chromium}"`);
        console.log(chalk.yellow('Opening browser...'), chromium, '\n');
        execSync(`osascript openChrome.applescript "${encodeURI(url)}" "${chromium}"`, { cwd: paths.scripts, stdio: 'ignore' });
        return true;
      } catch (error) {
        // console.log(chalk.red('openBrowser'), chromium, '\n', error);
      }
    }
  }

  // Another special case: on OS X, check if BROWSER has been set to "open".
  // In this case, instead of passing `open` to `opn` (which won't work),
  // just ignore it (thus ensuring the intended behavior, i.e. opening the system browser):
  // https://github.com/facebook/create-react-app/pull/1690#issuecomment-283518768
  if (process.platform === 'darwin' && browser === 'open') { browser = undefined; }
  // NOTE arguments must be passed as array with the browser
  if (typeof browser === 'string' && args.length > 0) { browser = [browser].concat(args); }

  // NOTE Fallback to open (It will always open new tab)
  try {
    open(url, { app: browser, wait: false, url: true }).catch(() => {});
    return true;
  } catch (error) { return false; }
}

/*****************************************************************
 * WEBPACK -> configure('development' | 'production' | 'test');
 * returns webpack configuration
 ****************************************************************/
export const configure = env => {
  console.log(chalk.cyan(`Configure "${env}" Wepback...`), '\n');
  const imageInlineSizeLimit = varNumber(process.env.IMAGE_INLINE_SIZE_LIMIT) || 10000;
  const shouldUseSourceMap = varBoolean(process.env.GENERATE_SOURCEMAP);

  return {
    // https://webpack.js.org/configuration/devtool/
    devtool: !shouldUseSourceMap ? void(0) : varString(process.env.GENERATE_SOURCEMAP),
    // https://webpack.js.org/configuration/entry-context/#entry
    entry: paths.index,
    // https://webpack.js.org/configuration/target/
    target: ['browserslist'],
    // https://webpack.js.org/configuration/stats/
    stats: 'errors-warnings',
    // https://webpack.js.org/configuration/mode
    mode: env === ENV.PROD ? 'production' : 'development',
    // https://webpack.js.org/configuration/other-options/#bail
    bail: env === ENV.PROD,
    // https://webpack.js.org/configuration/output/
    output: {
      // clean: env === ENV.PROD,
      clean: true,
      path: paths.build,
      pathinfo: env === ENV.DEV,
      publicPath: varString(process.env.PUBLIC_URL),
      assetModuleFilename: 'media/[name].[hash][ext]',
      filename: env === ENV.PROD ? 'js/[name].[contenthash:6].js' : env === ENV.DEV ? 'js/bundle.js' : void(0),
      chunkFilename: env === ENV.PROD ? 'js/[name].[contenthash:6].chunk.js' : env === ENV.DEV ? 'js/[name].chunk.js' : void(0),
      // TODO seems useless
      // Point sourcemap entries to original disk location (format as URL on Windows)
      // devtoolModuleFilenameTemplate: env === ENV.PROD
      //   ? info => path.relative(paths.src, info.absoluteResourcePath).replace(/\\/g, '/')
      //   : env === ENV.DEV ? (info => path.resolve(info.absoluteResourcePath).replace(/\\/g, '/')) : void(0),
    },
    // https://webpack.js.org/configuration/cache/
    cache: {
      store: 'pack',
      type: 'filesystem',
      cacheDirectory: paths.wpCache,
      buildDependencies: {
        config: [paths.scripts],
        defaultWebpack: ['webpack/lib/'],
      },
    },
    // https://webpack.js.org/configuration/other-options/#infrastructurelogging
    infrastructureLogging: { level: 'none' },
    // https://webpack.js.org/configuration/performance/
    performance: env === ENV.DEV ? false : {
      // NOTE take in mind the hints assumed all assets js and css at output as one entry point
      hints: 'warning',
      maxAssetSize: 1.5 * 1024 * 1024, // 1.5MB
      maxEntrypointSize: 3 * 1024 * 1024, // 3MB
    },
    // https://webpack.js.org/configuration/resolve/
    resolve: {
      modules: ['node_modules', paths.modules],
      // Do you want an alias ? Feel free to break your life ;)
      alias: { src: paths.src }
    },
    // https://webpack.js.org/configuration/optimization/
    optimization: {
      // https://webpack.js.org/plugins/split-chunks-plugin/
      // splitChunks: env !== ENV.PROD ? false : {
      //   cacheGroups: {
      //     vendor: {
      //       chunks: 'all',
      //       name: 'vendor',
      //       // TODO Something strange here - that will generate much more code than you may expect
      //       // maxSize: 512 * 1024, // 0.6MB
      //       // maxSize: 1024 * 1024, // 1MB
      //       test: /[\\/]node_modules[\\/]/,
      //     }
      //   },
      // },
      // enable for prod only
      minimize: env === ENV.PROD,
      minimizer: [
        // https://webpack.js.org/plugins/css-minimizer-webpack-plugin/
        new CssMinimizerWebpackPlugin(),
        // https://webpack.js.org/plugins/terser-webpack-plugin
        new TerserWebpackPlugin({
          extractComments: false,
          // https://webpack.js.org/plugins/terser-webpack-plugin/#terseroptions
          terserOptions: {
            parse: { ecma: 8 },
            mangle: { safari10: true },
            output: { ecma: 5, comments: false, ascii_only: true },
            compress: { ecma: 5, warnings: false, comparisons: false, inline: 2 },
          },
        }),
      ],
    },
    // https://webpack.js.org/concepts/modules/
    module: {
      strictExportPresence: true,
      rules: [
        // Handle node_modules packages that contain sourcemaps
        shouldUseSourceMap && {
          enforce: 'pre',
          exclude: /@babel(?:\/|\\{1,2})runtime/,
          test: /\.(js|mjs|jsx|ts|tsx|css)$/,
          loader: nodeModule('source-map-loader'),
        },
        {
          // https://webpack.js.org/configuration/module/#ruleoneof
          oneOf: [
            {
              type: 'asset',
              test: [/\.avif$/],
              mimetype: 'image/avif',
              parser: { dataUrlCondition: { maxSize: imageInlineSizeLimit } },
            },
            {
              type: 'asset',
              test: [/\.bmp$/, /\.gif$/, /\.jpe?g$/, /\.png$/],
              parser: { dataUrlCondition: { maxSize: imageInlineSizeLimit } },
            },
            // import starUrl, { ReactComponent as Star } from './star.svg'
            // https://www.npmjs.com/package/@svgr/webpack
            {
              test: /\.svg$/,
              issuer: { and: [/\.(js|jsx)$/] },
              use: [
                { loader: nodeModule('@svgr/webpack') },
                {
                  loader: nodeModule('file-loader'),
                  options: { name: 'media/[name].[hash].[ext]' },
                },
              ],
            },
            // Process application JS with Babel.
            // https://webpack.js.org/loaders/babel-loader/
            {
              test: /\.js$/,
              include: paths.src,
              exclude: /node_modules/,
              loader: nodeModule('babel-loader'),
              options: { compact: env === ENV.PROD },
            },
            // Process any JS outside of the app with Babel.
            // Unlike the application JS, we only compile the standard ES features.
            // {
            //   test: /\.js$/,
            //   exclude: /@babel(?:\/|\\{1,2})runtime/,
            //   loader: absolutePath('babel-loader'),
            //   options: {
            //     babelrc: false,
            //     configFile: false,
            //     compact: false,
            //     presets: [
            //       [
            //         absolutePath('babel-preset-react-app/dependencies'),
            //         { helpers: true },
            //       ],
            //     ],
            //     cacheDirectory: true,
            //     cacheCompression: false,
            //     sourceMaps: shouldUseSourceMap,
            //     inputSourceMap: shouldUseSourceMap,
            //   },
            // },
            // Style
            {
              // See https://github.com/webpack/webpack/issues/6571
              sideEffects: true,
              test: /\.css$/,
              use: [
                env === ENV.DEV && nodeModule('style-loader'),
                env === ENV.PROD && { loader: MiniCssExtractPlugin.loader },
                {
                  loader: nodeModule('css-loader'),
                  options: {
                    importLoaders: 1,
                    modules: { mode: 'icss' },
                    sourceMap: env === ENV.PROD ? shouldUseSourceMap : env === ENV.DEV,
                  },
                }
              ].filter(Boolean),
            },
            // Style preprocessor
            {
              // See https://github.com/webpack/webpack/issues/6571
              sideEffects: true,
              test: /\.(scss|sass)$/,
              use: [
                env === ENV.DEV && nodeModule('style-loader'),
                env === ENV.PROD && { loader: MiniCssExtractPlugin.loader },
                {
                  loader: nodeModule('css-loader'),
                  options: {
                    importLoaders: 1,
                    modules: { mode: 'icss' },
                    sourceMap: env === ENV.PROD ? shouldUseSourceMap : env === ENV.DEV,
                  },
                },
                {
                  loader: nodeModule('resolve-url-loader'),
                  options: {
                    root: paths.src,
                    sourceMap: env === ENV.PROD ? shouldUseSourceMap : env === ENV.DEV,
                  },
                },
                {
                  loader: nodeModule('sass-loader'),
                  options: { sourceMap: true },
                }
              ].filter(Boolean),
            },
            {
              type: 'asset/resource',
              exclude: [/^$/, /\.(js|mjs|jsx|ts|tsx)$/, /\.html$/, /\.json$/],
            },
            // ** STOP ** Are you adding a new loader?
            // IMPORTANT Make sure to add the new loader(s) before the "file" loader.
          ],
        },
      ].filter(Boolean),
    },
    // https://webpack.js.org/plugins/
    plugins: [
      // Generates an `index.html` file with the <script> injected.
      // https://webpack.js.org/plugins/html-webpack-plugin
      new HtmlWebpackPlugin({
        inject: true,
        template: paths.html,
        templateParameters: {
          PUBLIC_URL: process.env.PUBLIC_URL,
        },
        // NOTE minification options
        ...(env === ENV.PROD ? { minify: {
          removeComments: true,
          collapseWhitespace: true,
          removeRedundantAttributes: true,
          useShortDoctype: true,
          removeEmptyAttributes: true,
          removeStyleLinkTypeAttributes: true,
          keepClosingSlash: true,
          minifyJS: true,
          minifyCSS: true,
          minifyURLs: true,
        } } : {})
      }),
      // https://webpack.js.org/plugins/environment-plugin/#dotenvplugin
      new DotenvWebpack({ systemvars: true }),
      // https://webpack.js.org/plugins/mini-css-extract-plugin/
      env === ENV.PROD && new MiniCssExtractPlugin({
        filename: 'css/[name].[contenthash:6].css',
        chunkFilename: 'css/[name].[contenthash:6].chunk.css',
      }),
      // https://webpack.js.org/plugins/eslint-webpack-plugin/
      new ESLintWebpackPlugin({
        extensions: ['js', 'jsx'],
        lintDirtyModulesOnly: false,
        context: paths.src,
        eslintPath: nodeModule('eslint'),
        failOnError: varBoolean(process.env.ESLINT_NO_DEV_ERRORS),
        cache: true,
        cacheLocation: path.resolve(paths.wpCache, '.eslint/'),
        // ESLint class options
        cwd: paths.app,
        resolvePluginsRelativeTo: paths.scripts,
      }),
    ].filter(Boolean),
  };
};
