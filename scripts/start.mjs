
// outsource dependencies
import chalk from 'chalk';
import webpack from 'webpack';
import address from 'address';
import detectPort from 'detect-port';
import WebpackDevServer from 'webpack-dev-server';

// local dependencies
import { ENV, pkg, environment, checkRequiredFiles, syncBrowser, openBrowser, paths, configure, urlFrom, varBoolean } from './config.mjs';

// configure
const NODE_ENV = process.env.NODE_ENV = process.env.NODE_ENV || ENV.DEV;

environment(NODE_ENV);

checkRequiredFiles([paths.html, paths.index]);

syncBrowser()
  .then(() => detectPort(process.env.PORT, process.env.HOST))
  .then(port => {
    if (port != process.env.PORT) { throw new Error(`the PORT:${chalk.bold(process.env.PORT)} is Busy, Please try to define another port.`); }
  })
  .then(() => {
  const compiler = webpack(configure(NODE_ENV));
  // Was detected the Files just changed
  // compiler.hooks.invalid.tap('invalid', () => console.log('\n', chalk.cyanBright('Compiling...')));
  // Just finished recompiling the bundle
  compiler.hooks.done.tap('done', stats => {
    const { errors, warnings } = stats.toJson({ all: false, warnings: true, errors: true });
    (!errors.length && !warnings.length) && console.log(chalk.green('\n', 'Compiled successfully!'), '\n');
    if (errors.length) {
      console.log('\n', chalk.red('Compiled with errors.'), '\n');
    }
    if (warnings.length) {
      console.log('\n', chalk.yellow('Compiled with warnings.'), '\n');
      // console.log(
      //   warnings.map(warning => formatMessage(warning).message).join('\n\n'),
      //   '\n\n   Search for the', chalk.underline(chalk.yellow('keywords')), 'to learn more about each warning.',
      //   '\n   To ignore, add', chalk.cyan('// eslint-disable-next-line'), 'to the line before.\n'
      // );
    }
  });
  console.log(chalk.greenBright('V -'), chalk.green('Webpack ready'), '\n');
  return compiler;
}).then(compiler => {
  console.log(chalk.cyan('Starting development server...'), '\n');
  const server = new WebpackDevServer({
    headers: { 'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Methods': '*', 'Access-Control-Allow-Headers': '*' },
    allowedHosts: [process.env.HOST, address.ip()], // 'all'
    https: varBoolean(process.env.HTTPS),
    historyApiFallback: true, // entrypoint as fallback
    port: process.env.PORT,
    host: process.env.HOST,
    // liveReload: true,
    compress: true,
    open: false,
    // open: {
    //   app: {
    //     name: 'google-chrome',
    //     // arguments: ['--incognito', '--new-window'],
    //   },
    // },
    client: {
      logging: 'warn',
      reconnect: true,
      overlay: {
        errors: true,
        warnings: false,
      },
    },
    // TODO remove
    // onAfterSetupMiddleware: devServer => devServer.app.use((req, res, next) => console.log('devServer', req.url)),
  }, compiler);
  server.startCallback(() => {
    console.log(`\nYou can now view ${chalk.yellowBright(chalk.bold(pkg.name))} in the browser.`);
    console.log(`   ${chalk.bold('Local:')}            ${urlFrom({ hostname: process.env.HOST })}`);
    console.log(`   ${chalk.bold('On Your Network:')}  ${urlFrom({ hostname: address.ip() })}\n\n`);
    openBrowser(urlFrom({ hostname: process.env.HOST }));
  });
  // server.stopCallback(() => {
  //   console.log('Server stopped.');
  // });
  return server;
});
