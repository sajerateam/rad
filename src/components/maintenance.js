
// outsource dependencies
import cn from 'classnames';
import PropTypes from 'prop-types';
import React, { memo } from 'react';

// local dependencies
import { CogIcon } from './icon';

export const Maintenance = memo(function Maintenance ({ className }) {
  return <div className={cn('maintenance', className)}>
    <div className="text-center" style={{ width: 640, maxWidth: '95%' }}>
      <h1 className="mb-3 position-relative">
        <CogIcon icon="cog" size="3x" spin className="text-info" />
        <span className="position-absolute" style={{ left: '33%', top: '10%' }}>
          <CogIcon icon="cog" size="lg" spin className="text-success" />
        </span>
        <CogIcon icon="cog" size="5x" spin className="text-purple" />
        <CogIcon icon="cog" size="lg" spin className="text-warning ms-n2" />
      </h1>
      <h2> SITE IS UNDER MAINTENANCE </h2>
      <h5> We&#39;ll back online shortly! </h5>
    </div>
  </div>;
});
Maintenance.propTypes = {
  className: PropTypes.string,
};
Maintenance.defaultProps = {
  className: 'animated tada',
};
