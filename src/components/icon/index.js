
// outsource dependencies
import cn from 'classnames';
import PropTypes from 'prop-types';
import React, { memo, useMemo } from 'react';
import { fas } from '@fortawesome/free-solid-svg-icons';
// import { fab } from '@fortawesome/free-brands-svg-icons';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

// local dependencies
import * as app from './app-svg-icons';

/**
 * As main Icon component we will use the font-awesome really awesome component
 * @example
 * <Icon icon={['far', icon]} {...attr} />
 */
export const Fa = FontAwesomeIcon;
export default FontAwesomeIcon;

/**
 * Preparing icons
 * @param icon
 * @returns {React.NamedExoticComponent<object>}
 */
function create (src, name) {
  const icon = src[name];
  if (!icon || !icon.iconName || !icon.prefix) {
    throw new Error(`Invalid Icon ${name}`);
  }
  // NOTE add to fontawesome library
  library.add(icon);
  // NOTE create component to use in React
  const Icon = memo((props) => <FontAwesomeIcon icon={[icon.prefix, icon.iconName]} { ...props } />);
  // NOTE Name for profiler and test for component
  Icon.displayName = `${icon.prefix}-${icon.iconName}-icon`;
  return Icon;
}
// console.log('fas', fas);
// @see https://fontawesome.com/v5/search
export const CogIcon = create(fas, 'faCog');
export const ListIcon = create(fas, 'faList');
export const BarsIcon = create(fas, 'faBars');
export const PlusIcon = create(fas, 'faPlus');
export const TrashIcon = create(fas, 'faTrash');
export const TimesIcon = create(fas, 'faTimes');
export const SearchIcon = create(fas, 'faSearch');
export const ListAltIcon = create(fas, 'faListAlt');
export const UserCogIcon = create(fas, 'faUserCog');
export const SignOutIcon = create(fas, 'faSignOutAlt');
export const EllipsisHIcon = create(fas, 'faEllipsisH');
export const ExclamationTriangleIcon = create(fas, 'faExclamationTriangle');
export const SortIcon = create(fas, 'faSort');
export const SortAmountUpIcon = create(fas, 'faSortAmountUp');
export const SortAmountDownIcon = create(fas, 'faSortAmountDown');

//
// export const SendIcon = create(app.send);
export const EditIcon = create(app, 'edit');
export const EnvelopIcon = create(app, 'envelope');

/********************************************************************************************************/
export const HeartFillIcon = create(app, 'heartFill');
export const HeartOutlineIcon = create(app, 'heartOutline');
// NOTE Icon with addition functionality
export const FavHeartToggle = memo(function FavHeartToggle ({ status, statusMap, className, ...attr }) {
  const Icon = useMemo(() => statusMap(status), [statusMap, status]);
  return <Icon { ...attr } className={cn('', className)} />;
});
FavHeartToggle.propTypes = {
  status: PropTypes.any,
  statusMap: PropTypes.func,
  className: PropTypes.string,
};
FavHeartToggle.defaultProps = {
  status: null,
  className: '',
  statusMap: status => {
    switch (status) {
      default: return HeartOutlineIcon;
      case true: return HeartFillIcon;
      case false: return HeartOutlineIcon;
    }
  }
};
