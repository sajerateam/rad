
// outsource dependencies
import _ from 'lodash';
import cn from 'classnames';
import PropTypes from 'prop-types';
import { Label } from 'reactstrap';
import React, { memo } from 'react';
import { Form, reduxForm } from 'redux-form';

// local dependencies
import { AlertError } from './alert-error';

/**
 *
 */
export const RFError = ({ meta }) => !meta.touched ? null : <AlertError message={meta.error} />;

/**
 * Show form error using prepared label
 */
export const LabelError = memo(function LabelError ({ message, htmlFor, className }) {
  return !message ? null : <Label htmlFor={htmlFor} className={cn('invalid-feedback d-block', className)}>
    { _.isString(message) ? message : JSON.stringify(message, null, 4) }
  </Label>;
});
LabelError.propTypes = {
  htmlFor: PropTypes.string,
  className: PropTypes.string,
  message: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
};
LabelError.defaultProps = {
  htmlFor: null,
  message: null,
  className: null,
};

/**
 * Show form error using prepared error components (bottom label or Popover)
 */
export const ReduxFormControl = memo(function ReduxFormControl ({ label, className, message, id, children, ...attr }) {
  // NOTE represented as a div with "mb-3" class name what impossible to override
  // return <FormGroup { ...attr } className={className}>
  return <div { ...attr } className={className}>
    { !label ? null : <Label htmlFor={id}> { label } </Label> }
    { children }
    { _.isArray(message)
      ? message.map(item => <LabelError key={item} htmlFor={id} message={item} />)
      : <LabelError htmlFor={id} message={message} /> }
  </div>;
});
ReduxFormControl.propTypes = {
  ...LabelError.propTypes,
  className: PropTypes.string,
  label: PropTypes.oneOfType([PropTypes.node, PropTypes.string, PropTypes.element]),
};
ReduxFormControl.defaultProps = {
  ...LabelError.defaultProps,
  className: 'mb-3',
  label: null,
};

/**
 * Simplify usage of FieldArray to handle most common situations
 */
export const ReduxFormArrayItems = memo(function ReduxFormArrayItems ({ Item, meta, fields, className, ...attr }) {
  const errors = meta.error;
  return <div className={cn('rf-array-items', className)}>
    { meta.error && <LabelError message={errors} /> }
    { fields.map((k, i) => <Item
      { ...attr }
      key={i}
      field={k}
      index={i}
      value={fields.get(i)}
    />) }
  </div>;
});
ReduxFormArrayItems.propTypes = {
  className: PropTypes.string,
  meta: PropTypes.object.isRequired,
  fields: PropTypes.object.isRequired,
  Item: PropTypes.elementType.isRequired,
};
ReduxFormArrayItems.defaultProps = {
  className: 'mb-3',
};

const FormContent = memo(function FormContent ({ onSubmit, handleSubmit, autoComplete, children, className, id }) {
  return <Form
    // NOTE prepare submit
    onSubmit={handleSubmit(onSubmit)}
    // NOTE map allowed for DOM
    { ...({ autoComplete, children, className, id }) }
  />
});
FormContent.propTypes = {
  className: PropTypes.string,
  autoComplete: PropTypes.string,
  onSubmit: PropTypes.func.isRequired,
  children: PropTypes.node.isRequired,
  // handleSubmit: PropTypes.func.isRequired, // NOTE provided by redux-form HOC
};
FormContent.defaultProps = {
  autoComplete: 'off',
  className: null,
};

export const ReduxForm = reduxForm({
  enableReinitialize: true,
// NOTE map allowed "reduxForm" HOC pass a lot of properties not allowed for DOM nodes
})(FormContent);
ReduxForm.propTypes = {
  ...FormContent.propTypes,
  validate: PropTypes.func,
  initialValues: PropTypes.object,
  form: PropTypes.string.isRequired,
};
ReduxForm.defaultProps = {
  ...FormContent.defaultProps,
  initialValues: {},
  validate: void(0),
};
