
// outsource dependencies
import cn from 'classnames';
import PropTypes from 'prop-types';
import React, { memo } from 'react';
import { Link } from 'react-router-dom';
import { Container, Row, Col, Button } from 'reactstrap';

// local dependencies
import { NotFoundImg } from '../images';

export const NotFound = memo(function NotFound ({ fallback, className, message }) {
  return <div id="NotFound" className={cn('not-found', className)}>
    <Container className="pt-5">
      <Row>
        <Col xs="12" md="6" lg={{ size: 5, offset: 1 }} className="content-holder">
          <h2 className="title m-0"> 404 </h2>
          <h3 className="description mb-5"> { message } </h3>
          <Button size="lg" tag={Link} to={fallback} color="primary" className="rounded-pill pr-5 pl-5">
            <strong> Go Home </strong>
          </Button>
        </Col>
        <Col xs="12" md="6" lg="5">
          <NotFoundImg />
        </Col>
      </Row>
    </Container>
  </div>;
});
NotFound.propTypes = {
  message: PropTypes.string,
  fallback: PropTypes.string,
  className: PropTypes.string,
};
NotFound.defaultProps = {
  fallback: '/',
  className: 'animated tada',
  message: 'Something went WRONG!',
};
