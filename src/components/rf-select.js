
// outsource dependencies
import _ from 'lodash';
import cn from 'classnames';
import Select from 'react-select';
import PropTypes from 'prop-types';
import CreatableSelect from 'react-select/creatable';
import React, { memo, useCallback, useEffect, useMemo } from 'react';

// local dependencies
import { ReduxFormControl } from './redux-form';
import { useRefCallback } from '../module/hooks';

// eslint-disable-next-line max-len
export const RFSelect = memo(function RFSelect ({ input, meta, label, skipTouch, classNameFormGroup, isCreatable, className, disabled, actionInputChange, clearOnUnmount, isSimple, defaultValue, getOptionValue, options, isMulti, ...attr }) {
  const { name, value, onChange, onBlur } = input;
  let message = '', statusClass = '';
  if (skipTouch || meta.touched) {
    message = meta.error;
    statusClass += meta.valid ? ' is-valid' : ' is-invalid';
  }
  // NOTE controlled feature to clearing redux form field value on component unmount
  useEffect(() => clearOnUnmount ? () => onChange(null) : void 0, [onChange, clearOnUnmount]);
  const handleInputChange = useCallback((text, prevAction) => {
    if (!_.isFunction(actionInputChange)) { return; }
    if (_.get(prevAction, 'prevInputValue') === text) { return; }
    actionInputChange({ text });
  }, [actionInputChange]);
  // NOTE to allow form control touches
  const Component = useMemo(() => isCreatable ? CreatableSelect : Select, [isCreatable]);
  // NOTE provide ability to set into form only value from option object
  const handleChange = useCallback(
    newValue => isSimple ? onChange(isMulti ? _.map(newValue, getOptionValue) : getOptionValue(newValue)) : onChange(newValue),
    [onChange, getOptionValue, isSimple, isMulti]
  );
  const handleBlur = useCallback(() => onBlur(value), [onBlur, value]);
  const val = useMemo(
    () => isSimple ? (isMulti ? getMultiValue : getValue)(options, value, getOptionValue) : value,
    [options, value, getOptionValue, isSimple, isMulti]
  );
  // NOTE in case clearing value via redux we might faced with cached value within Select
  const [select, ref] = useRefCallback();
  useEffect(() => {
    if (_.isUndefined(val) && select && !value) {
      select.clearValue();
    }
  }, [val, value, select]);

  return <ReduxFormControl
    id={name}
    label={label}
    message={message}
    className={cn('rf-select', statusClass, classNameFormGroup)}
  >
    <Component
      // menuIsOpen
      ref={ref}
      inputId={name}
      onInputChange={handleInputChange}
      { ...attr }
      value={val}
      name={name}
      isMulti={isMulti}
      options={options}
      onBlur={handleBlur}
      isDisabled={disabled}
      onChange={handleChange}
      getOptionValue={getOptionValue}
      classNamePrefix="rf-select-field"
      className={cn('rf-select-field', className)}
    />
  </ReduxFormControl>;
});
RFSelect.propTypes = {
  ...ReduxFormControl.propTypes,
  disabled: PropTypes.bool,
  isSimple: PropTypes.bool, // allow to put into redux form only value from options item
  skipTouch: PropTypes.bool,
  className: PropTypes.string,
  getOptionLabel: PropTypes.func,
  getOptionValue: PropTypes.func,
  clearOnUnmount: PropTypes.bool,
  closeMenuOnSelect: PropTypes.bool,
  meta: PropTypes.object.isRequired,
  actionInputChange: PropTypes.func,
  input: PropTypes.object.isRequired,
  options: PropTypes.array.isRequired,
  classNameFormGroup: ReduxFormControl.propTypes.className,
};
RFSelect.defaultProps = {
  ...ReduxFormControl.defaultProps,
  className: '',
  disabled: false,
  isSimple: false,
  skipTouch: false,
  clearOnUnmount: false,
  closeMenuOnSelect: true,
  actionInputChange: null,
  classNameFormGroup: ReduxFormControl.defaultProps.className,
  getOptionLabel: item => _.get(item, 'label', '...'),
  getOptionValue: item => _.get(item, 'value'),
};

function getValue (options, value, getOptionValue) {
  // NOTE incorrect checks lead to impossible to select false value as 0|null|false
  if (_.isUndefined(value)) { return value; }
  return _.find(options, o => getOptionValue(o) === value);
}

function getMultiValue (options, value, getOptionValue) {
  console.log('%c getMultiValue ', 'color: #FF6766; font-weight: bolder;'
    , '\n options:', options
    , '\n value:', value
  );
  return _.map(value, o => getValue(options, o, getOptionValue));
}
