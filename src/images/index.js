
// outsource dependencies
import cn from 'classnames';
import PropTypes from 'prop-types';
import React, { memo, useState, useCallback } from 'react';

// local dependencies
import logo from './logo.svg';
import upload from './upload.svg';
import defImg from './def-image.svg';
import notFound from './not-found.png';
import logoSmall from './logo-small.svg';
import settingsGif from './settings.gif';
import defAvatar from './def-avatar.svg';
import { APP_TITLE } from '../constants';

// eslint-disable-next-line max-len
export const DefImage = memo(function DefImage ({ src, defaultSrc, defaultAlt, defaultTitle, defaultStyle, style, className, defaultClassName, ...attr }) {
  const [hasError, setHasError] = useState(false);
  const handleError = useCallback(() => setHasError(true), []);

  return <img
    alt={defaultAlt}
    title={defaultTitle}
    { ...attr }
    onError={handleError}
    style={Object.assign({}, defaultStyle, style)}
    src={(hasError ? defaultSrc : src) || defaultSrc}
    className={cn('img-fluid', defaultClassName, className)}
  />;
});
DefImage.propTypes = {
  src: PropTypes.string,
  style: PropTypes.object,
  className: PropTypes.string,
  defaultSrc: PropTypes.string,
  defaultAlt: PropTypes.string,
  defaultTitle: PropTypes.string,
  defaultStyle: PropTypes.object,
  defaultClassName: PropTypes.string,
};
DefImage.defaultProps = {
  src: null,
  style: {},
  className: null,
  defaultSrc: defImg,
  defaultTitle: null,
  defaultAlt: 'image',
  defaultClassName: null,
  defaultStyle: {},
};

export const Avatar = memo(function Avatar (props) {
  return <DefImage
    defaultAlt="User"
    defaultTitle="User"
    defaultSrc={defAvatar}
    defaultClassName="avatar rounded-circle"
    { ...props }
  />;
});

export const CloudImage = memo(function CloudImage (props) {
  return <DefImage
    defaultSrc={upload}
    defaultAlt="Upload to cloud"
    defaultTitle="Upload to cloud"
    defaultClassName="cloud-image"
    { ...props }
  />;
});

export const SettingGif = memo(function SettingGif (props) {
  return <DefImage
    defaultAlt="Settings"
    defaultTitle="Settings"
    defaultSrc={settingsGif}
    defaultClassName="settings-gif"
    { ...props }
  />;
});

export const NotFoundImg = memo(function NotFound (props) {
  return <DefImage
    defaultAlt="404"
    defaultSrc={notFound}
    defaultClassName="not-found-png"
    { ...props }
  />;
});

export const Logo = memo(function Logo (props) {
  return <DefImage
    defaultSrc={logo}
    defaultAlt={APP_TITLE}
    defaultTitle={APP_TITLE}
    defaultClassName="logo"
    { ...props }
  />;
});

export const LogoSmall = memo(function LogoSmall (props) {
  return <DefImage
    defaultSrc={logoSmall}
    defaultAlt={APP_TITLE}
    defaultTitle={APP_TITLE}
    defaultClassName="logo-small"
    { ...props }
  />;
});
