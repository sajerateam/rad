
// outsource dependencies
import { create } from 'redux-saga-controller';
import { takeEvery, put, call } from 'redux-saga/effects';

// local dependencies
import { config } from '../../constants';
import { API, silence } from '../../services';

// configure
export const emailConfirmationCtrl = create({
  prefix: 'email-confirmation',
  actions: {
    initialize: 'INITIALIZE',
  },
  initial: {
    disabled: false,
    initialized: false,
    errorMessage: null,
    isTokenValid: false,
  },
  subscriber: function * () {
    yield takeEvery(emailConfirmationCtrl.action.initialize.TYPE, silence, initializeExe);
  }
});

function * initializeExe ({ type, payload }) {
  config('DEBUG') && console.info(`%c${type} `, 'color: #FF6766; font-weight: bolder;'
    , '\n initializeExe:'
    , '\n payload:', payload
  );
  let isTokenValid;
  try {
    yield call(API, { method: 'POST', url: '/auth/token/confirmation', data: payload });
    isTokenValid = true;
  } catch ({ message }) {
    isTokenValid = false;
  }
  // NOTE do nothing
  yield put(emailConfirmationCtrl.action.updateCtrl({ initialized: true, isTokenValid }));
}
