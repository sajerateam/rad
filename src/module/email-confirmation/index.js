
// outsource dependencies
import cn from 'classnames';
import React, { memo, useEffect } from 'react';
import { Container, Row, Col } from 'reactstrap';
import { Link, useParams } from 'react-router-dom';
import { useController } from 'redux-saga-controller';

// local dependencies
import { Logo } from '../../images';
import { SIGN_IN } from '../../constants';
import { Preloader } from '../../components';
import { emailConfirmationCtrl } from './controller';

// configure

export const EmailConfirmation = memo(function EmailConfirmation ({ className }) {
  const { token } = useParams();
  const [
    { initialized, isTokenValid },
    { initialize },
  ] = useController(emailConfirmationCtrl);
    // NOTE initialize business logic
  useEffect(() => { initialize({ token }); }, [initialize, token]);

  return <Preloader active={!initialized}>
    <div id="EmailConfirmation" className={cn('d-flex align-items-center justify-content-center h-100', className)}>
      <Container fluid style={{ width: 390, maxWidth: '95%' }}>
        <Row className="align-items-center">
          <Col xs="12" className="text-center pt-3 mb-3">
            <Logo className="img-fluid" style={{ width: 100 }} />
            <h3 className="pt-1 text-center text-primary"> RAD Dummy </h3>
          </Col>
          <Col xs="12" tag="h3" className="text-center mb-4"> Email verification </Col>
          { !isTokenValid ? <Col tag="h5" xs={{ size: 10, offset: 1 }} className="text-center text-danger mb-4">
            Whoa there! The request token for this page is invalid.
            It may have already been used, or expired because it is too old.
            Please go back and try again.
            <br/><br/>
            <small className="text-muted"> it was probably just a mistake </small>
          </Col> : <Col tag="h4" xs={{ size: 10, offset: 1 }} className="text-center text-success mb-4">
            The email address was successfully verified. Welcome aboard !
          </Col> }
          <Col xs="12" className="text-center mb-3">
            <Link to={SIGN_IN.LINK()}> Sign In </Link>
          </Col>
        </Row>
      </Container>
    </div>
  </Preloader>;
});
