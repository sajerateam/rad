
// outsource dependencies
import _ from 'lodash';
import { useCallback, useState } from 'react';
import { useControllerActions, useControllerData } from 'redux-saga-controller';

// local dependencies
import { appRootCtrl } from './controller';

/**
 * logged user or null if it was not logged
 */
export const useSelf = () => {
  const { user } = useControllerData(appRootCtrl);
  return user;
};

/**
 * provide functionality for aside within app
 */
export const useToggleAside = () => {
  const { updateCtrl } = useControllerActions(appRootCtrl);
  const { aside } = useControllerData(appRootCtrl);
  const handleToggle = useCallback(
    value => updateCtrl({ aside: _.isBoolean(value) ? value : !aside }),
    [updateCtrl, aside]
  );
  return [aside, handleToggle];
};

/**
 * correct extract ref to provide ability use ref with "useEffect" hook
 */
export const useRefCallback = () => {
  const [stored, set] = useState(null);
  // NOTE prevent update "reference" within render
  const ref = useCallback(api => api && set(api), []);
  return [stored, ref];
};

/**
 * simple prepared boolean and toggle fn
 */
export const useToggle = initial => {
  const [value, set] = useState(Boolean(initial));
  return [value, useCallback(() => set(current => !current), [])];
};
