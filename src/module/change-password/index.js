
// outsource dependencies
import * as yup from 'yup';
import cn from 'classnames';
import { Field } from 'redux-form';
import { Link, useParams } from 'react-router-dom';
import { useController } from 'redux-saga-controller';
import { Container, Row, Button, Col } from 'reactstrap';
import React, { memo, useCallback, useEffect } from 'react';

// local dependencies
import { Logo } from '../../images';
import { changePasswordCtrl, FORM_NAME } from './controller';
import { ReduxForm, RFInput, Preloader, Spinner, AlertError } from '../../components';
import { FORGOT_PASSWORD, SIGN_IN, VALID, createYupSyncValidator } from '../../constants';

// configure
const formValidation = createYupSyncValidator(yup.object().shape({
  confirmPassword: VALID.CONFIRM_PASSWORD.required('Password confirmation is mandatory'),
  password: VALID.PASSWORD.required('Password is mandatory'),
}));

export const ChangePassword = memo(function ChangePassword ({ className }) {
  const { token } = useParams();
  const [
    { initialized, disabled, errorMessage, isTokenValid },
    { initialize, updateData, updateCtrl },
  ] = useController(changePasswordCtrl);
    // NOTE initialize business logic
  useEffect(() => { initialize({ token }); }, [initialize, token]);
  // NOTE prepare page actions
  const clearError = useCallback(() => updateCtrl({ errorMessage: null }), [updateCtrl]);

  return <Preloader active={!initialized}>
    <ReduxForm
      form={FORM_NAME}
      id="ChangePassword"
      onSubmit={updateData}
      validate={formValidation}
      className={cn('d-flex align-items-center justify-content-center h-100', className)}
    >
      <Container fluid style={{ width: 390, maxWidth: '95%' }}>
        <Row>
          <Col xs="12" className="text-center pt-3 mb-3">
            <Logo className="img-fluid" style={{ width: 100 }} />
            <h3 className="pt-1 text-center text-primary"> RAD Dummy </h3>
          </Col>
          { !isTokenValid ? <Col xs={{ size: 10, offset: 1 }} tag="h5" className="text-center text-danger mb-4">
            Whoa there! The request token for this page is invalid.
            It may have already been used, or expired because it is too old.
            Please go back to the
            <Link to={FORGOT_PASSWORD.LINK()}> forgot password page </Link>
            and try again.
            <br/><br/>
            <p className="text-center">
              <small className="text-muted"> it was probably just a mistake </small>
            </p>
          </Col> : <>
            <Col xs={{ size: 10, offset: 1 }}>
              <Field
                name="password"
                type="password"
                component={RFInput}
                disabled={disabled}
                placeholder="Password"
                label={<strong className="required-asterisk"> Password </strong>}
              />
              <Field
                type="password"
                component={RFInput}
                disabled={disabled}
                name="confirmPassword"
                placeholder="Confirm password"
                label={<strong className="required-asterisk"> Confirm Password </strong>}
              />
              <Button
                block
                outline
                type="submit"
                color="primary"
                disabled={disabled}
                className="mb-3 rounded-pill"
              >
                Change password
                <Spinner active={disabled} />
              </Button>
              <AlertError active message={errorMessage} onClear={clearError}/>
            </Col>
            <Col xs={{ size: 5, offset: 1 }} className="mb-3">
              <Link to={SIGN_IN.LINK()}> Sign In </Link>
            </Col>
            <Col xs="5" className="text-end mb-3">
              <Link to={FORGOT_PASSWORD.LINK()}> Forgot Password ? </Link>
            </Col>
          </> }
        </Row>
      </Container>
    </ReduxForm>
  </Preloader>;
});
