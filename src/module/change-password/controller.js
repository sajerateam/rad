
// outsource dependencies
import { toastr } from 'react-redux-toastr';
import { updateSyncErrors } from 'redux-form';
import { create } from 'redux-saga-controller';
import { takeEvery, put, call, delay } from 'redux-saga/effects';

// local dependencies
import { API, silence } from '../../services';
import { config, SIGN_IN } from '../../constants';

// configure
export const FORM_NAME = 'CHANGE_PASSWORD';
export const changePasswordCtrl = create({
  prefix: FORM_NAME,
  actions: {
    initialize: 'INITIALIZE',
    updateData: 'UPDATE_DATA'
  },
  initial: {
    token: null,
    disabled: false,
    initialized: false,
    errorMessage: null,
    isTokenValid: false,
  },
  subscriber: function * () {
    yield takeEvery(changePasswordCtrl.action.initialize.TYPE, silence, initializeExe);
    yield takeEvery(changePasswordCtrl.action.updateData.TYPE, silence, updateDataExe);
  }
});

function * initializeExe ({ type, payload }) {
  config('DEBUG') && console.info(`%c${type} `, 'color: #FF6766; font-weight: bolder;'
    , '\n initializeExe:'
    , '\n payload:', payload
  );
  let isTokenValid;
  try {
    yield call(API, { method: 'POST', url: '/auth/token/forgot-password/exists', data: payload });
    isTokenValid = true;
  } catch ({ message }) {
    isTokenValid = false;
  }
  // NOTE do nothing
  yield put(changePasswordCtrl.action.updateCtrl({ initialized: true, isTokenValid, ...payload }));
}

function * updateDataExe ({ type, payload }) {
  config('DEBUG') && console.info(`%c${type} `, 'color: #FF6766; font-weight: bolder;'
    , '\n updateDataExe:'
    , '\n payload:', payload
  );
  yield put(changePasswordCtrl.action.updateCtrl({ disabled: true, errorMessage: null }));
  try {
    // TODO implement
    yield call(API, { method: 'POST', url: '/auth/token/change-password', data: payload });
    yield call(toastr.success, 'Change password', 'Password was successfully changed');
    yield delay(3 * 1000);
    yield call(SIGN_IN.PUSH, {});
  } catch ({ message }) {
    yield call(toastr.error, 'Error', message);
    yield put(changePasswordCtrl.action.updateCtrl({ errorMessage: message }));
    yield put(updateSyncErrors(FORM_NAME, { confirmPassword: ' ' }));
  }
  yield put(changePasswordCtrl.action.updateCtrl({ disabled: false }));
}
