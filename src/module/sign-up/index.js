
// outsource dependencies
import * as yup from 'yup';
import cn from 'classnames';
import { Field } from 'redux-form';
import { Link } from 'react-router-dom';
import { useController } from 'redux-saga-controller';
import { Container, Row, Button, Col } from 'reactstrap';
import React, { memo, useCallback, useEffect } from 'react';

// local dependencies
import { Logo } from '../../images';
import { signUpCtrl, FORM_NAME } from './controller';
import { FORGOT_PASSWORD, SIGN_IN, VALID, createYupSyncValidator } from '../../constants';
import { ReduxForm, RFInput, Preloader, Spinner, AlertError, RFCheckbox } from '../../components';


// configure
const formValidation = createYupSyncValidator(yup.object().shape({
  confirmPassword: VALID.CONFIRM_PASSWORD.required('Password confirmation is mandatory'),
  password: VALID.PASSWORD.required('Password is mandatory'),
  email: VALID.EMAIL.required('Email is mandatory'),
  name: VALID.NAME.required('Name is mandatory'),
  privacyPolicy: VALID.PRIVACY_POLICY,
}));

export const SignUp = memo(function SignUp ({ className }) {
  const [
    { initialized, disabled, errorMessage },
    { initialize, updateData, updateCtrl },
  ] = useController(signUpCtrl);
    // NOTE initialize business logic
  useEffect(() => { initialize(); }, [initialize]);
  // NOTE prepare page actions
  const clearError = useCallback(() => updateCtrl({ errorMessage: null }), [updateCtrl]);

  return <Preloader active={!initialized}>
    <ReduxForm
      id="SignUp"
      form={FORM_NAME}
      onSubmit={updateData}
      validate={formValidation}
      className={cn('d-flex align-items-center justify-content-center h-100', className)}
    >
      <Container fluid style={{ width: 390, maxWidth: '95%' }}>
        <Row>
          <Col xs="12" className="text-center pt-3 mb-3">
            <Logo className="img-fluid" style={{ width: 100 }} />
            <h3 className="pt-1 text-center text-primary"> RAD Dummy </h3>
          </Col>
          <Col xs={{ size: 10, offset: 1 }}>
            <Field
              name="name"
              type="text"
              component={RFInput}
              disabled={disabled}
              placeholder="Username"
              label={<strong className="required-asterisk"> Name </strong>}
            />
            <Field
              type="text"
              name="email"
              component={RFInput}
              disabled={disabled}
              placeholder="Email"
              label={<strong className="required-asterisk"> Email Address </strong>}
            />
            <Field
              name="password"
              type="password"
              component={RFInput}
              disabled={disabled}
              placeholder="Password"
              label={<strong className="required-asterisk"> Password </strong>}
            />
            <Field
              type="password"
              component={RFInput}
              disabled={disabled}
              name="confirmPassword"
              placeholder="Confirmation"
              label={<strong className="required-asterisk"> Confirm Password </strong>}
            />
            <Field
              disabled={disabled}
              name="privacyPolicy"
              component={RFCheckbox}
              label={<span className="privacy-policy">
                I agree to the
                <Link to="terms"> Terms </Link>
                and acknowledge our
                <Link to="policy"> Privacy Policy. </Link>
              </span>}
            />
            <Button
              block
              outline
              type="submit"
              color="primary"
              disabled={disabled}
              className="mb-3 rounded-pill"
            >
              <span> Sign Up </span>
              <Spinner active={disabled} />
            </Button>
            <AlertError active message={errorMessage} onClear={clearError}/>
          </Col>
          <Col xs="12" className="text-center mb-3">
            <Link to={SIGN_IN.LINK()}>
              <strong> Sign In </strong>
            </Link>
          </Col>
          <Col xs="12" className="text-center mb-3">
            <Link to={FORGOT_PASSWORD.LINK()}>
              <strong> Forgot your password ? </strong>
            </Link>
          </Col>
        </Row>
      </Container>
    </ReduxForm>
  </Preloader>;
});
