
// outsource dependencies
import { toastr } from 'react-redux-toastr';
import { updateSyncErrors } from 'redux-form';
import { create } from 'redux-saga-controller';
import { takeEvery, put, call, delay } from 'redux-saga/effects';

// local dependencies
import { API, silence } from '../../services';
import { config, SIGN_IN } from '../../constants';

// configure
export const FORM_NAME = 'SIGN_UP';
export const signUpCtrl = create({
  prefix: FORM_NAME,
  actions: {
    initialize: 'INITIALIZE',
    updateData: 'UPDATE_DATA'
  },
  initial: {
    disabled: false,
    initialValues: {},
    initialized: false,
    errorMessage: null,
  },
  subscriber: function * () {
    yield takeEvery(signUpCtrl.action.initialize.TYPE, silence, initializeExe);
    yield takeEvery(signUpCtrl.action.updateData.TYPE, silence, updateDataExe);
  }
});

function * initializeExe ({ type, payload }) {
  config('DEBUG') && console.log(`%c${type} `, 'color: #FF6766; font-weight: bolder;'
    , '\n payload:', payload
  );
  // NOTE do nothing
  yield put(signUpCtrl.action.updateCtrl({ initialized: true }));
}

function * updateDataExe ({ type, payload }) {
  // config('DEBUG') && console.log(`%c${type} `, 'color: #FF6766; font-weight: bolder;'
  //   , '\n payload:', payload
  // );
  yield put(signUpCtrl.action.updateCtrl({ disabled: true, errorMessage: null }));
  try {
    // TODO implement
    yield call(API, { data: payload, method: 'POST', url: '/auth/token/sign-up' });
    yield call(toastr.success, 'Sign Up', 'Data was successfully sent');
    yield delay(2 * 1000);
    yield call(SIGN_IN.PUSH, {});
  } catch ({ message }) {
    yield call(toastr.error, 'Error', message);
    yield put(signUpCtrl.action.updateCtrl({ errorMessage: message }));
    yield put(updateSyncErrors(FORM_NAME, { name: ' ' }));
  }
  yield put(signUpCtrl.action.updateCtrl({ disabled: false }));
}
