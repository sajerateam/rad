
// outsource dependencies
import { create } from 'redux-saga-controller';
import { takeEvery, put, call, delay, select } from 'redux-saga/effects';

// local dependencies
import { API, silence } from '../services';
import { config, store } from '../constants';

// configure
API.onAuthFailApplicationAction(() => store.dispatch(appRootCtrl.action.signOut({})));

export const appRootCtrl = create({
  prefix: 'app',
  actions: {
    initialize: 'INITIALIZE',
    signOut: 'SIGN_OUT',
  },
  initial: {
    initialized: false,   // prevent redirect from page and show instead global preloader
    health: true,         // prevent redirect from page and show instead maintenance page
    user: null,           // logged user information
    aside: false,         // collapse/expand aside
  },
  subscriber: function * () {
    yield takeEvery(appRootCtrl.action.signOut.TYPE, silence, signOutExe);
    yield takeEvery(appRootCtrl.action.initialize.TYPE, silence, initializeExe);
  }
});

function * initializeExe ({ type, payload }) {
  // yield put(appRootCtrl.action.clearCtrl());
  config('DEBUG') && console.log(`%c${type} `, 'color: #FF6766; font-weight: bolder;'
    , '\n payload:', payload
  );
  // NOTE check health of API
  try {
    const { status } = { status: 'UP' };
    // const { status } = yield call(instancePUB, { method: 'GET', url: '/actuator/health' });
    // NOTE API may answer "DOWN" (not ready yet)
    if (status !== 'UP') { throw new Error('API down for maintenance'); }
    yield put(appRootCtrl.action.updateCtrl({ health: true }));
  } catch ({ message: error1 }) {
    yield put(appRootCtrl.action.updateCtrl({ health: false }));
    // NOTE try again another time
    yield delay(10 * 1000);
    yield put(appRootCtrl.action.initialize({}));
    return;
  }

  const hasSession = yield call(API.hasStoredSession);
  // NOTE try to restore user auth
  if (hasSession) {
    try {
      yield call(API.restoreSessionFromStore);
      yield call(getSelfExe, {});
    } catch ({ message: error2 }) {
      yield call(signOutExe, {});
    }
  }
  // NOTE initialization done
  yield put(appRootCtrl.action.updateCtrl({ initialized: true }));
}

function * signOutExe ({ type, payload }) {
  config('DEBUG') && console.log(`%c${type} `, 'color: #FF6766; font-weight: bolder;'
    , '\n payload:', payload
  );
  // NOTE clear client side session from store
  yield call(API.setupSession, null);
  const { user } = yield select(appRootCtrl.select);
  if (!user) { return null; }
  yield put(appRootCtrl.action.updateCtrl({ user: null }));
  // NOTE use silence helper if you don't want to handle error
  yield call(silence, API, { method: 'POST', url: '/auth/logout' });
}

export function * getSelfExe ({ type, payload }) {
  const user = { name: 'I am a fake user data' };
  // const user = yield call(API, { method: 'GET', url: 'auth/users/me' });
  yield put(appRootCtrl.action.updateCtrl({ user }));
}
