
// outsource dependencies
import React, { memo } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

// local dependencies
import { MENU } from './menu';
import { Users } from './users';
import { Layout } from './layout';
import { useSelf } from '../hooks';
import { Welcome } from './welcome';
import { NotFound } from '../../components';
import { ERROR_MESSAGE } from '../../services';
import * as ROUTES from '../../constants/routes';

export const Authorized = memo(function Authorized () {
  const user = useSelf();

  return !user
    ? <Redirect to={ROUTES.SIGN_IN.LINK({}, { error: ERROR_MESSAGE.FORBIDDEN })}/>
    : <Layout menu={MENU}>
      <Switch>
        <Route path={ROUTES.USERS_LIST.ROUTE} component={Users} />
        <Route path={ROUTES.WELCOME_SCREEN.ROUTE} component={Welcome} />
        {/*OTHERWISE*/}
        <Route path="" component={NotFound} />
        {/*<Redirect to={{ pathname: ROUTES.WELCOME_SCREEN.LINK() }} />*/}
      </Switch>
    </Layout>;
});
