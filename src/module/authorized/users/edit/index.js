
// outsource dependencies
import * as yup from 'yup';
import { useSelector } from 'react-redux';
import React, { memo, useEffect } from 'react';
import { Field, isPristine } from 'redux-form';
import { Link, useParams } from 'react-router-dom';
import { useController } from 'redux-saga-controller';
import { Container, Row, Col, Button } from 'reactstrap';

// local dependencies
import { userEditCtrl, FORM_NAME } from './controller';
import { USERS_LIST, VALID, createYupSyncValidator } from '../../../../constants';
import { AlertError, Preloader, ReduxForm, RFInput, RFSelect } from '../../../../components';

// configure
const rolesOptions = [{ label: 'Admin', value: 'ADMIN' }, { label: 'User', value: 'USER' }];
const formValidation = createYupSyncValidator(yup.object().shape({
  firstName: VALID.NAME.required('First name is mandatory'),
  emailAddress: VALID.EMAIL.required('Email is mandatory'),
  lastName: VALID.NAME.required('Last name is mandatory'),
  roles: VALID.ENTITY_LIST.required('Role is mandatory'),
}));

export const UserEdit = memo(function UserEdit () {
  const [
    { initialized, initial, isNew, disabled, errorMessage },
    { initialize, updateUser }
  ] = useController(userEditCtrl);
  const pristine = useSelector(isPristine(FORM_NAME));
  const { id } = useParams();

  useEffect(() => { initialize({ id }); }, [initialize, id]);

  return <Container id="UserEdit" className="user-edit">
    <Preloader active={!initialized} className="vh-100">
      <Row>
        <Col xs="12" className="mb-3">
          <h2> { isNew ? 'Create' : 'Edit' } User </h2>
        </Col>
      </Row>
      <Row>
        <Col xs={{ size: 8, offset: 2 }}>
          <ReduxForm
            form={FORM_NAME}
            onSubmit={updateUser}
            initialValues={initial}
            validate={formValidation}
          >
            <Field
              type="text"
              name="firstName"
              component={RFInput}
              disabled={disabled}
              placeholder="First name"
            />
            <Field
              type="text"
              name="lastName"
              component={RFInput}
              disabled={disabled}
              placeholder="Last name"
            />

            <Field
              type="email"
              name="emailAddress"
              component={RFInput}
              disabled={disabled}
              placeholder="Email"
            />
            <Field
              isMulti
              skipTouch
              name="roles"
              placeholder="Roles"
              disabled={disabled}
              component={RFSelect}
              options={rolesOptions}
            />
            <AlertError message={errorMessage} />
            <Row className="mb-3">
              <Col xs="12" className="d-flex justify-content-between">
                <Button
                  tag={Link}
                  color="danger"
                  to={USERS_LIST.LINK()}
                  disabled={disabled}
                  className="rounded-pill mr-2"
                >
                  BACK
                </Button>
                <Button
                  type="submit"
                  color="primary"
                  className="rounded-pill"
                  disabled={disabled || pristine}
                >
                  SAVE
                </Button>
              </Col>
            </Row>
          </ReduxForm>
        </Col>
      </Row>
    </Preloader>
  </Container>;
});
