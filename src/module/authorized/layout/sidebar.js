
// outsource dependencies
import cn from 'classnames';
import PropTypes from 'prop-types';
import React, { memo } from 'react';
import { Link, useLocation } from 'react-router-dom';
import { ProSidebar, Menu, MenuItem, SubMenu, SidebarHeader, SidebarContent } from 'react-pro-sidebar';

// local dependencies
import { useToggleAside } from '../../hooks';

// Spec available menu item types
export const MENU_ITEM_TYPE = {
  MENU: 'MENU',
  LINK: 'LINK',
  HEADER: 'HEADER',
  ACTION: 'ACTION',
};

export const Sidebar = memo(function SideBarMenu ({ menu, className }) {
  // NOTE required to rerender each time when location change
  const location = useLocation();
  const [expanded] = useToggleAside();

  return <ProSidebar id="Sidebar" collapsed={!expanded} className={cn('sidebar', className)}>
    <SidebarContent>
      <Menu iconShape="square">
        { (menu || []).map(({ key, name, type, link, isActive, Icon, action, disabled, list }, index) =>
          type !== MENU_ITEM_TYPE.MENU
            ? <ItemType key={key || index} { ...({ name, type, link, isActive, Icon, action, disabled, list }) } />
            : <SubMenu
              key={key || index}
              popperarrow
              title={name}
              disabled={disabled}
              icon={Icon && <Icon />}
              className={cn({ 'pe-none': disabled })}
              defaultOpen={isActive && isActive(location)}
            >
              { (list || []).map((item, index) => <ItemType key={item.key || index} { ...item } />) }
            </SubMenu>) }
      </Menu>
    </SidebarContent>
  </ProSidebar>;
});
const ItemType = memo(function ItemType ({ type, Icon, name, link, action, isActive, disabled }) {
  // NOTE required to rerender each time when location change
  const location = useLocation();

  switch (type) {
    default: return null;
    case MENU_ITEM_TYPE.LINK: return <MenuItem
      icon={Icon && <Icon />}
      active={isActive && isActive(location)}
      className={cn({ 'pe-none': disabled })}
    >
      <Link to={link}> { name } </Link>
    </MenuItem>;
    case MENU_ITEM_TYPE.ACTION: return <MenuItem
      onClick={action}
      icon={Icon && <Icon />}
      active={isActive && isActive(location)}
      className={cn({ 'pe-none': disabled })}
    >
      { name }
    </MenuItem>;
    case MENU_ITEM_TYPE.HEADER: return <SidebarHeader>
      <h4 className="m-2 text-center"> { name } </h4>
    </SidebarHeader>;
  }
});
ItemType.propTypes = {
  type: PropTypes.oneOf(Object.values(MENU_ITEM_TYPE)).isRequired,
  name: PropTypes.string.isRequired,
  isActive: PropTypes.func,
  action: PropTypes.func,
  link: PropTypes.string,
};
ItemType.defaultProps = {
  isActive: () => false,
  action: null,
  link: null,
};
Sidebar.propTypes = {
  menu: PropTypes.arrayOf(PropTypes.shape(ItemType.propTypes)).isRequired,
  className: PropTypes.string,
};
Sidebar.defaultProps = {
  className: null,
};
