
// outsource dependencies
import cn from 'classnames';
import PropTypes from 'prop-types';
import React, { memo, useMemo } from 'react';
import { Link } from 'react-router-dom';
import { Nav, NavItem, Navbar, NavbarBrand, Button } from 'reactstrap';

// local dependencies
import { UserMenu } from './user-menu';
import { useToggleAside } from '../../hooks';
import { BarsIcon } from '../../../components';
import { Logo, LogoSmall } from '../../../images';
import { APP_TITLE, WELCOME_SCREEN } from '../../../constants';

export const Header = memo(function Header ({ className }) {
  const [expanded, toggleAside] = useToggleAside();

  const Image = useMemo(() => expanded ? Logo : LogoSmall, [expanded]);

  return <Navbar id="Header" dark color="dark" className={cn('header', className, { expanded, collapsed: !expanded })}>
    <NavbarBrand tag={Link} to={WELCOME_SCREEN.LINK()}>
      <Image className="logo" alt={APP_TITLE} />
    </NavbarBrand>
    <Nav className="flex-grow-1">
      <NavItem className="flex-grow-0">
        <Button color="none" onClick={toggleAside}>
          <BarsIcon size="lg" />
        </Button>
      </NavItem>
      <NavItem className="d-flex flex-grow-1">
        {/* actually it empty - fill free to implement more header features */}
      </NavItem>
      <NavItem className="flex-grow-0">
        <UserMenu className="" />
      </NavItem>
    </Nav>
  </Navbar>;
});
Header.propTypes = {
  className: PropTypes.string
};
Header.defaultProps = {
  className: null
};
