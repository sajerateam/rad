
// outsource dependencies
import _ from 'lodash';
import cn from 'classnames';
import PropTypes from 'prop-types';
import React, { memo, useEffect, useRef } from 'react';

// local dependencies
import { Header } from './header';
import { Sidebar } from './sidebar';
import { useToggleAside } from '../../hooks';

export const Layout = memo(function Layout ({ className, children, menu }) {
  const [expanded, toggleAside] = useToggleAside();

  // NOTE auto collapse menu
  const handleResize = useRef();
  useEffect(() => {
    handleResize.current = _.throttle(() => {
      if (expanded && window.innerWidth < 768) {
        toggleAside(false);
      } else if (!expanded && window.innerWidth > 768) {
        toggleAside(true);
      }
    }, 5e2, { trailing: true });
  }, [expanded, toggleAside]);
  useEffect(() => { handleResize.current(); }, []);
  useEffect(() => {
    window.addEventListener('resize', handleResize.current);
    return () => window.removeEventListener('resize', handleResize.current);
  }, [expanded, toggleAside]);

  return <div id="Layout" className={cn('layout', { collapsed: !expanded }, className)}>
    <Header expanded={expanded} />
    <div className="d-flex vh-100">
      <Sidebar menu={menu} expanded={expanded} />
      <main id="content">
        <div className="hide-scroll-bar">
          { children }
        </div>
      </main>
    </div>
  </div>;
});
Layout.propTypes = {
  children: PropTypes.element.isRequired,
  className: PropTypes.string
};
Layout.defaultProps = {
  className: null
};
