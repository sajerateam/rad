
// outsource dependencies
import React, { memo } from 'react';
import { Container, Row, Col } from 'reactstrap';

// local dependencies
import { useSelf } from '../../hooks';
import { Logo } from '../../../images';
import { APP_TITLE } from '../../../constants';

export const Welcome = memo(function Welcome () {
  const { name } = useSelf();

  return <Container id="Welcome" fluid>
    <h4 className="pt-3 text-center"> Welcome to { APP_TITLE }! </h4>
    <h5 className="text-center"> { name || 'My Friend'} </h5>
    <hr className="row" />
    <Row>
      <Col xs={{ size: 10, offset: 1 }} className="text-center">
        <Logo className="mb-0" />
        <h1 className="text-center text-primary"> { APP_TITLE } </h1>
      </Col>
    </Row>
  </Container>;
});
