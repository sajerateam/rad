
// outsource dependencies
import _ from 'lodash';

// local dependencies
import { NEW_ID } from '../../constants';
import * as ROUTES from '../../constants/routes';
import { MENU_ITEM_TYPE } from './layout/sidebar';
import { ListIcon, ListAltIcon, PlusIcon, ExclamationTriangleIcon } from '../../components';

export const MENU = [
  {
    Icon: ListAltIcon,
    name: 'Welcome screen',
    type: MENU_ITEM_TYPE.LINK,
    link: ROUTES.WELCOME_SCREEN.LINK(),
    isActive: ROUTES.WELCOME_SCREEN.TEST,
  }, {
    name: 'Users',
    Icon: ListIcon,
    type: MENU_ITEM_TYPE.MENU,
    // NOTE ability to custom detection of active state
    isActive: ROUTES.USERS_LIST.TEST,
    list: [
      {
        name: 'Add user',
        Icon: PlusIcon,
        type: MENU_ITEM_TYPE.ACTION,
        action: () => ROUTES.USERS_EDIT.PUSH({ id: NEW_ID }),
        isActive: () => ROUTES.USERS_EDIT.TEST() && ROUTES.USERS_EDIT.PARAMS().id === NEW_ID,
      }, {
        name: 'List',
        Icon: ListAltIcon,
        type: MENU_ITEM_TYPE.LINK,
        link: ROUTES.USERS_LIST.LINK(),
        isActive: () => ROUTES.USERS_LIST.TEST() && !ROUTES.USERS_EDIT.TEST(),
      }, {
        name: 'Some Action',
        type: MENU_ITEM_TYPE.ACTION,
        Icon: ExclamationTriangleIcon,
        action: () => alert('Addition action ᕦ(ツ)ᕤ from menu ¯\\(ヅ)/¯'),
      }
    ]
  }, {
    key: 'custom-key-example',
    name: 'More examples',
    type: MENU_ITEM_TYPE.HEADER,
  }, {
    name: '404',
    type: MENU_ITEM_TYPE.LINK,
    link: `${ROUTES.AUTHORIZED}/absent/rout`,
  }, {
    type: MENU_ITEM_TYPE.LINK,
    name: 'Users',
    link: ROUTES.USERS_LIST.LINK(),
    isActive: () => ROUTES.USERS_EDIT.TEST
  }, {
    disabled: true,
    name: 'Disabled link',
    type: MENU_ITEM_TYPE.LINK,
    link: ROUTES.WELCOME_SCREEN.LINK(),
  }, {
    disabled: true,
    name: 'Disabled action',
    type: MENU_ITEM_TYPE.ACTION,
    action: () => console.log('Newer fire'),
  }, {
    Icon: ListIcon,
    name: 'View menu',
    type: MENU_ITEM_TYPE.MENU,
    list: [
      {
        key: 'custom-key-example-nested-item',
        name: 'Users',
        type: MENU_ITEM_TYPE.LINK,
        link: ROUTES.USERS_LIST.LINK(),
        isActive: () => ROUTES.USERS_LIST.TEST(),
      }, {
        type: MENU_ITEM_TYPE.LINK,
        name: 'Welcome',
        link: ROUTES.WELCOME_SCREEN.LINK(),
        isActive: () => ROUTES.WELCOME_SCREEN.TEST(),
      }, {
        type: MENU_ITEM_TYPE.ACTION,
        name: 'Console fire',
        action: () => console.log('fire =)', _.size(MENU)),
      }
    ]
  }, {
    disabled: true,
    type: MENU_ITEM_TYPE.ACTION,
    name: 'Types and Properties',
    action: () => console.log('Newer fire'),
    // NOTE ability to custom detection of active state
    isActive: () => true
  }
];
