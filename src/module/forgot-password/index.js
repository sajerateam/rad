
// outsource dependencies
import * as yup from 'yup';
import cn from 'classnames';
import { Field } from 'redux-form';
import { Link } from 'react-router-dom';
import { useController } from 'redux-saga-controller';
import { Container, Row, Button, Col } from 'reactstrap';
import React, { memo, useCallback, useEffect } from 'react';

// local dependencies
import { Logo } from '../../images';
import { forgotPasswordCtrl, FORM_NAME } from './controller';
import { SIGN_IN, SIGN_UP, VALID, createYupSyncValidator } from '../../constants';
import { ReduxForm, RFInput, Preloader, Spinner, AlertError } from '../../components';

// configure
const formValidation = createYupSyncValidator(yup.object().shape({
  email: VALID.EMAIL.required('Email is required'),
}));

export const ForgotPassword = memo(function ForgotPassword ({ className }) {
  const [
    { initialized, disabled, errorMessage },
    { initialize, updateData, updateCtrl },
  ] = useController(forgotPasswordCtrl);
    // NOTE initialize business logic
  useEffect(() => { initialize(); }, [initialize]);
  // NOTE prepare page actions
  const clearError = useCallback(() => updateCtrl({ errorMessage: null }), [updateCtrl]);

  return <Preloader active={!initialized}>
    <ReduxForm
      form={FORM_NAME}
      id="ForgotPassword"
      onSubmit={updateData}
      validate={formValidation}
      className={cn('d-flex align-items-center justify-content-center h-100', className)}
    >
      <Container fluid style={{ width: 380, maxWidth: '95%' }}>
        <Row className="align-items-center">
          <Col xs="12" className="text-center pt-3 mb-2">
            <Logo className="img-fluid" style={{ width: 100 }} />
            <h3 className="pt-1 text-center text-primary"> RAD Dummy </h3>
          </Col>
          <Col xs="12" tag="h2" className="text-center text-info"> Forgot password ? </Col>
          <Col xs="12" tag="p" className="text-center text-muted mb-4">
            Please enter your email address, and we&apos;ll send you a password reset email.
          </Col>
          <Col xs={{ size: 10, offset: 1 }}>
            <Field
              type="text"
              name="email"
              component={RFInput}
              disabled={disabled}
              placeholder="Email Address"
            />
            <Button
              block
              outline
              type="submit"
              color="primary"
              disabled={disabled}
              className="mb-3 rounded-pill"
            >
              Reset Password
              <Spinner active={disabled} />
            </Button>
            <AlertError active message={errorMessage} onClear={clearError}/>
          </Col>
          <Col xs={{ size: 5, offset: 1 }} className="text-start mb-3">
            <Link to={SIGN_IN.LINK()}>
              <strong> Sign In </strong>
            </Link>
          </Col>
          <Col xs="5" className="text-end mb-3">
            <Link to={SIGN_UP.LINK()}>
              <strong> Sign Up </strong>
            </Link>
          </Col>
        </Row>
      </Container>
    </ReduxForm>
  </Preloader>;
});
