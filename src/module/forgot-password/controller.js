
// outsource dependencies
import { toastr } from 'react-redux-toastr';
import { updateSyncErrors } from 'redux-form';
import { create } from 'redux-saga-controller';
import { takeEvery, put, call } from 'redux-saga/effects';

// local dependencies
import { API, silence } from '../../services';
import { config, SIGN_IN } from '../../constants';

// configure
export const FORM_NAME = 'FORGOT_PASSWORD';
export const forgotPasswordCtrl = create({
  prefix: FORM_NAME,
  actions: {
    initialize: 'INITIALIZE',
    updateData: 'UPDATE_DATA'
  },
  initial: {
    disabled: false,
    initialValues: {},
    initialized: false,
    errorMessage: null,
  },
  subscriber: function * () {
    yield takeEvery(forgotPasswordCtrl.action.initialize.TYPE, silence, initializeExe);
    yield takeEvery(forgotPasswordCtrl.action.updateData.TYPE, silence, updateDataExe);
  }
});

function * initializeExe ({ type, payload }) {
  config('DEBUG') && console.info(`%c${type} `, 'color: #FF6766; font-weight: bolder;'
    , '\n initializeExe:'
    , '\n payload:', payload
  );
  // NOTE do nothing
  yield put(forgotPasswordCtrl.action.updateCtrl({ initialized: true }));
}

function * updateDataExe ({ type, payload }) {
  config('DEBUG') && console.info(`%c${type} `, 'color: #FF6766; font-weight: bolder;'
    , '\n updateDataExe:'
    , '\n payload:', payload
  );
  yield put(forgotPasswordCtrl.action.updateCtrl({ disabled: true, errorMessage: null }));
  try {
    // TODO implement
    yield call(API, { method: 'POST', url: '/auth/token/forgot-password', payload });
    yield call(toastr.success, 'Reset password', 'Please check your email.');
    yield call(SIGN_IN.PUSH, {});
  } catch ({ message }) {
    yield call(toastr.error, 'Error', message);
    yield put(forgotPasswordCtrl.action.updateCtrl({ errorMessage: message }));
    yield put(updateSyncErrors(FORM_NAME, { email: ' ' }));
  }
  yield put(forgotPasswordCtrl.action.updateCtrl({ disabled: false }));
}
