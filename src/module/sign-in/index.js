
// outsource dependencies
import * as yup from 'yup';
import cn from 'classnames';
import { Field } from 'redux-form';
import { Link } from 'react-router-dom';
import { useController } from 'redux-saga-controller';
import { Container, Row, Button, Col } from 'reactstrap';
import React, { memo, useCallback, useEffect } from 'react';

// local dependencies
import { Logo } from '../../images';
import { signInCtrl, FORM_NAME } from './controller';
import { ReduxForm, RFInput, Spinner, Preloader, AlertError } from '../../components';
import { FORGOT_PASSWORD, SIGN_UP, VALID, createYupSyncValidator } from '../../constants';


// configure
const formValidation = createYupSyncValidator(yup.object().shape({
  password: VALID.STRING.required('Password is Required'),
  username: VALID.STRING.required('Login is Required'),
}));

export const SignIn = memo(function SignIn ({ className }) {
  const [
    { initialized, disabled, errorMessage },
    { initialize, updateData, updateCtrl }
  ] = useController(signInCtrl);
    // NOTE initialize business logic
  useEffect(() => { initialize(); }, [initialize]);
  // NOTE prepare page actions
  const clearError = useCallback(() => updateCtrl({ errorMessage: null }), [updateCtrl]);

  return <Preloader active={!initialized}>
    <ReduxForm
      id="SignIn"
      form={FORM_NAME}
      onSubmit={updateData}
      validate={formValidation}
      className={cn('d-flex align-items-center justify-content-center h-100', className)}
    >
      <Container fluid style={{ width: 390, maxWidth: '95%' }}>
        <Row>
          <Col xs="12" className="text-center pt-3 mb-3">
            <Logo className="img-fluid" style={{ width: 100 }} />
            <h3 className="pt-1 text-center text-primary"> RAD Dummy </h3>
          </Col>
          <Col xs={{ size: 10, offset: 1 }}>
            <Field
              type="text"
              name="username"
              component={RFInput}
              disabled={disabled}
              placeholder="Login"
            />
            <Field
              name="password"
              type="password"
              component={RFInput}
              disabled={disabled}
              placeholder="Password"
            />
            <Button
              block
              outline
              type="submit"
              color="primary"
              disabled={disabled}
              className="mb-3 rounded-pill"
            >
              <span> LOGIN </span>
              <Spinner active={disabled} />
            </Button>
            <AlertError active message={errorMessage} onClear={clearError}/>
          </Col>
          <Col xs="12" className="text-center mb-3">
            <Link to={SIGN_UP.LINK()}>
              <strong> Sign Up </strong>
            </Link>
          </Col>
          <Col xs="12" className="text-center mb-3">
            <Link to={FORGOT_PASSWORD.LINK()}>
              <strong> Forgot your password ? </strong>
            </Link>
          </Col>
        </Row>
      </Container>
    </ReduxForm>
  </Preloader>;
});
