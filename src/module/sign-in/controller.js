
// outsource dependencies
import { toastr } from 'react-redux-toastr';
import { updateSyncErrors } from 'redux-form';
import { create } from 'redux-saga-controller';
import { takeEvery, put, call } from 'redux-saga/effects';

// local dependencies
import { getSelfExe } from '../controller';
import { API, silence } from '../../services';
import { config, WELCOME_SCREEN, SIGN_IN } from '../../constants';

// configure
export const FORM_NAME = 'SIGN_IN';
export const signInCtrl = create({
  prefix: FORM_NAME,
  actions: {
    initialize: 'INITIALIZE',
    updateData: 'UPDATE_DATA'
  },
  initial: {
    disabled: false,
    initialValues: {},
    initialized: false,
    errorMessage: null,
  },
  subscriber: function * () {
    yield takeEvery(signInCtrl.action.initialize.TYPE, silence, initializeExe);
    yield takeEvery(signInCtrl.action.updateData.TYPE, silence, updateDataExe);
  }
});

function * initializeExe ({ type, payload }) {
  const { error } = yield call(SIGN_IN.QUERY);
  config('DEBUG') && console.info(`%c ${type} `, 'color: #FF6766; font-weight: bolder; font-size: 12px;'
    , '\n payload:', payload
    , '\n error:', error
  );
  if (error) {
    yield put(signInCtrl.action.updateCtrl({ errorMessage: error }));
    yield call(toastr.error, 'Error', error);
    yield call(SIGN_IN.REPLACE);
  }
  // NOTE no extra action for initialization
  yield put(signInCtrl.action.updateCtrl({ initialized: true }));
}

function * updateDataExe ({ type, payload }) {
  // config('DEBUG') && console.log(`%c${type} `, 'color: #FF6766; font-weight: bolder;'
  //   , '\n payload:', payload
  // );
  yield put(signInCtrl.action.updateCtrl({ disabled: true, errorMessage: null }));
  try {
    // TODO implement
    const session = { access_token: 'JWT_access_token', refresh_token: 'JWT_refresh_token' };
    // const session = yield call(API, { data: payload, method: 'POST', url: '/auth/token' });
    yield call(API.setupSession, session);
    yield call(getSelfExe, {});
    yield call(WELCOME_SCREEN.PUSH, {});
    yield call(toastr.success, 'Welcome', 'We pleasure to see you');
  } catch ({ message }) {
    yield call(toastr.error, 'Error', message);
    yield call(API.setupSession, null);
    yield put(signInCtrl.action.updateCtrl({ errorMessage: message }));
    yield put(updateSyncErrors(FORM_NAME, { password: ' ', username: ' ' }));
  }
  yield put(signInCtrl.action.updateCtrl({ disabled: false }));
}
