
// outsource dependencies
import React from 'react';
import dayjs from 'dayjs';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import DJRelativeTimePlugin from 'dayjs/plugin/relativeTime';

// STYLES inject ...
import './style';
// local dependencies
import { App } from './module';
import { store } from './constants';

// NOTE extend "dayjs" by plugins
dayjs.extend(DJRelativeTimePlugin);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
