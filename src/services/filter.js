
// outsource dependencies

// local dependencies

/**
 *
 * @param {String} string
 * @returns {String}
 */
export const humanize = string => !string ? '' : String(string)
  // from camel case
  .replace(/([A-Z])([A-Z])([a-z])|([a-z])([A-Z])/g, '$1$4 $2$3$5')
  // spec
  .replace(/[_-]+/g, ' ')
  // normalize
  .replace(/\s+/g, ' ')
  // trim
  .replace(/^\s*|\s*$/g, '')
  // capitalize
  .toLowerCase()
  .replace(/^.{1,1}/, sib => sib.toUpperCase());

/**
 * formatting:cut string by options
 *
 * @param {String} string
 * @param {Object} [options] - truncate.defaultOptions
 * @returns {String}
 */
export const truncate = (string = '', options) => {
  const { length, end, breakOnWord } = Object.assign({}, truncate.defaultOptions, options);
  // NOTE skip cases
  if (isNaN(length) || length <= 0) { return ''; }
  if (typeof string !== 'string') { return ''; }
  // NOTE short enough
  if (string.length <= length) { return string; }
  // NOTE cut source
  string = string.substring(0, length);
  // NOTE cut more to the spice symbol
  if (!breakOnWord) {
    const lastSpace = string.lastIndexOf(' ');
    // NOTE get last space
    if (lastSpace !== -1) {
      string = string.substr(0, lastSpace);
    }
  }
  return string.trim() + end;
};
/********************************
 *  default configuration
 ********************************/
truncate.defaultOptions = {
  breakOnWord: false,
  length: 12,
  end: '...'
};

/**
 * handle string and make enum from it
 *
 * @param {String} string
 * @returns {String}
 */
export const toEnum = (string = '') => String(string)
  .replace(/[^\w\d\s]/gi, '')
  .replace(/[\s]+/g, '_')
  .replace(/^_+|_+$/g, '')
  .toUpperCase();

/**
 * formatting html to plain text
 *
 * @param {String} html
 * @returns {String}
 */
export const escapeHtml = (html = '') => String(html).replace(/<[^>]*>?/gm, '');

/**
 * trim text input
 * @param {String} value
 * @return {string}
 */
export const trim = value => String(value).trim();

/**
 * formatting number to duration string
 *
 * @param {Number} number
 * @param {Object} [options]
 * @returns {String}
 */
export const duration = (number = 0, options) => {
  let { format, regDay, regHour, regMin, regSec } = Object.assign({}, duration.defaultOptions, options);
  number = typeof number === 'number' ? Math.abs(number) : 0;
  format = typeof format === 'string' ? format : `${regDay}d ${regHour}h ${regMin}m ${regSec}s`;
  let days = 0,
    hours = 0,
    minutes = 0;

  if (new RegExp(regDay).test(format) && number >= duration.equal.days) {
    days = Math.floor(number / duration.equal.days);
    number -= (days * duration.equal.days);
  }

  if (new RegExp(regHour).test(format) && number >= duration.equal.hours) {
    hours = Math.floor(number / duration.equal.hours);
    number -= (hours * duration.equal.hours);
  }

  if (new RegExp(regMin).test(format) && number >= duration.equal.minutes) {
    minutes = Math.floor(number / duration.equal.minutes);
    number -= (minutes * duration.equal.minutes);
  }
  return format
    .replace(regDay, days)
    .replace(regHour, hours)
    .replace(regMin, minutes)
    .replace(regSec, number);
};
/********************************
 *  default configuration
 ********************************/
duration.defaultOptions = {
  // output format
  format: '[D]d [H]h [M]m [S]s',
  // regular expression to parse day
  regDay: '[D]',
  // regular expression to parse hour
  regHour: '[H]',
  // regular expression to parse minute
  regMin: '[M]',
  // regular expression to parse second
  regSec: '[S]',
};
duration.equal = {
  // day 24*60*60=86400
  days: 86400,
  // hour 60*60=3600
  hours: 3600,
  // minute 60
  minutes: 60,
  // second 1
  seconds: 1,
};
