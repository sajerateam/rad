
// outsource dependencies
import React from 'react';
import { Provider } from 'react-redux';
import { render, screen } from '@testing-library/react';

// local dependencies
import { App } from './module';
import { store, APP_TITLE } from './constants';

test('Should be alive', () => {
  const container = render(<Provider store={store}> <App /> </Provider>);
  container.debug();
  const element = screen.getByText(new RegExp(APP_TITLE, 'i'));
  console.log(`%c${'APP Should be alive'} `, 'color: #FF6766; font-weight: bolder;'
    , '\n element:', element
  );
  expect(element).toBeInTheDocument();
});
