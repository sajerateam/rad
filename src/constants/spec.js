
export const NEW_ID = 'new';

export const APP_TITLE = 'RAD';

export const IMG_DIR = {
  DEFAULT: 'DEFAULT',
  RECIPE: 'RECIPE',
  FOOD: 'FOOD',
  USER: 'USER',
};

export const STATUS = {
  DISABLED: 'DISABLED',
  ENABLED: 'ENABLED',
  DRAFT: 'DRAFT',
};

export const USER_STATE = {
  INACTIVE: 'INACTIVE',     // user was deleted from the system
  CREATED: 'CREATED',       // user created but does not finish sign up wizard
  PENDING: 'PENDING',       // user was created pass sign up but does not confirm email
  ACTIVE: 'ACTIVE',         // user was created pass sign up and confirm email
};
